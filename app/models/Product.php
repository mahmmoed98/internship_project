<?php
class Product
{
    private $db;

    // Initialize database object
    public function __construct()
    {
        $this->db = new Database;
    }
    // Get all products
    public function getProducts()
    {
        $this->db->query('SELECT products.*, product_attributes.attribut_name FROM products INNER JOIN product_attributes ON products.id=product_attributes.product_id ORDER BY products.id DESC');

        $results = $this->db->resultSet();

        return $results;
    }

    // Add new product
    public function addProduct($params)
    {

        $this->db->query('SELECT COUNT(*) as check_sku FROM products WHERE products.sku = :sku');
        $this->db->bind(':sku', $params['sku']);
        $this->db->execute();
        $sku = $this->db->single()->check_sku;
        // return $data->check_sku;
        if($sku == 0){

        $this->db->query('INSERT INTO products (sku, name, price, product_type) VALUES (:sku, :name, :price, :product_type)');

        $this->db->bind(':sku', $params['sku']);
        $this->db->bind(':name', $params['name']);
        $this->db->bind(':price', $params['price']);
        $this->db->bind(':product_type', $params['productType']);

        $lastInsertId = $this->db->execute();

        $productType = $params['productType']; 
        
        // Get attributes of a specific Product Type
        $data['attribut_name'] = (new Products)->Type(new $productType, $params);

        $this->db->query('INSERT INTO product_attributes (attribut_name, product_id) VALUES (:attribut_name, :product_id)');

        $this->db->bind(':attribut_name', $data['attribut_name']);
        $this->db->bind(':product_id', $lastInsertId);
        
       return $this->db->execute();
        } else {
            return "Error";
        }
    }

    // Delete product
    public function deleteProduct($id)
    {
        $this->db->query('DELETE FROM products WHERE id = :id');
        $this->db->bind(':id', $id);

        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }
}
