<?php

class Products extends Controller
{
public function __construct()
{
$this->productModel = $this->model('Product');
}

public function index()
{
$products = $this->productModel->getProducts();

$data = [
    'products' => $products
];

$this->view('products/index', $data);
}

public function Type(Attributes $type, $params)
{
$productAttributes = $type->getAttributes($params);
return $productAttributes;
}


public function addproduct()
{
// if Save button click
if (isset($_POST['name'])) {

    $params['sku'] = htmlspecialchars($_POST['sku']);
    $params['name'] = htmlspecialchars($_POST['name']);
    $params['price'] = htmlspecialchars($_POST['price']);
    $params['productType'] = htmlspecialchars($_POST['productType']);
    $params['width'] = htmlspecialchars($_POST['width']);
    $params['height'] = htmlspecialchars($_POST['height']);
    $params['length'] = htmlspecialchars($_POST['length']);
    $params['size'] = htmlspecialchars($_POST['size']);
    $params['weight'] = htmlspecialchars($_POST['weight']);

    if($this->productModel->addProduct($params) != "Error"){

    // Return to product listing page
?><script>
        window.location = 'https://juniortest-mahmmoed-alkotob.000webhostapp.com';
    </script><?php
                exit;
            } 
        }

            $this->view('products/addproduct');
        }

        public function delete()
        {
            foreach ($_POST['deleteID'] as $id) {
                $id = htmlspecialchars($id);
                $this->productModel->deleteProduct($id);
            }
        }
    }


    // ProductType abstract class
    abstract class Attributes
    {
        abstract function getAttributes($params);
    }

    class Forniture extends Attributes
    {

        public function getAttributes($params)
        {

            return 'Dimensions: ' . $params['width'] . '*' . $params['height'] . '*' . $params['length'];
        }
    }

    class Dvd extends Attributes
    {

        public function getAttributes($params)
        {

            return 'Size: ' . $params['size'] . ' MB';
        }
    }

    class Book extends Attributes
    {

        public function getAttributes($params)
        {

            return 'Weight: ' . $params['weight'] . ' KG';
        }
    }
