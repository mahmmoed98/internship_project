<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Product List | MVC OOP Project</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <!-- Libraries Stylesheet -->
    <link href="<?php echo URLROOT ?>/public/lib/animate/animate.min.css" rel="stylesheet">

    <!-- Customized Bootstrap Stylesheet -->
    <link href="<?php echo URLROOT ?>/public/css/bootstrap.min.css" rel="stylesheet">

    <!-- Template Stylesheet -->
    <link href="<?php echo URLROOT ?>/public/css/style.css" rel="stylesheet">

</head>

<body>
    <div class="container-xxl bg-white p-0">
        <!-- Spinner Start -->
        <div id="spinner" class="show bg-white position-fixed translate-middle w-100 vh-100 top-50 start-50 d-flex align-items-center justify-content-center">
            <div class="spinner-border text-primary" style="width: 3rem; height: 3rem;" role="status">
            </div>
        </div>
        <!-- Spinner End -->


        <!-- Navbar Start -->
        <div class="container-xxl  p-0">
            <nav class="navbar navbar-expand-lg navbar-light px-4 px-lg-5 py-3 py-lg-0 sticky-top shadow-sm">
                <a href="<?php echo URLROOT ?>" class="navbar-brand p-0">
                    <h1 class="m-0"><i class="fa fa-server me-3"></i>MVC-Project</h1>
                    <!-- <img src="img/logo.png" alt="Logo"> -->
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse">
                    <span class="fa fa-bars"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <div class="navbar-nav ms-auto py-0">
                        <a href="<?php echo URLROOT ?>/products/addProduct" class="btn btn-primary active">ADD</a>
                        <button type="button" class="btn btn-danger" disabled id="delete-checkbox" style="margin-left: 5px;">MASS DELETE</button>
                    </div>
                </div>
            </nav>

        </div>
        <!-- Navbar End -->