<!-- Back to Top -->
<a href="#" class="btn btn-lg btn-secondary btn-lg-square back-to-top">^</a>
</div>

<!-- JavaScript Libraries -->
<script src="<?php echo URLROOT ?>/public/js/jquery.min.js"></script>
<script src="<?php echo URLROOT ?>/public/lib/wow/wow.min.js"></script>
<script src="<?php echo URLROOT ?>/public/lib/easing/easing.min.js"></script>
<script src="<?php echo URLROOT ?>/public/lib/waypoints/waypoints.min.js"></script>
<script src="<?php echo URLROOT ?>/public/lib/counterup/counterup.min.js"></script>
<!-- <script src="lib/owlcarousel/owl.carousel.min.js"></script> -->

<!-- Template Javascript -->
<script src="<?php echo URLROOT ?>/public/js/main.js"></script>
</body>

</html>