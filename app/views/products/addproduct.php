<?php require APPROOT . '/views/includes/header.php'; ?>

<!-- Product Start -->
<div class="container-xxl py-5">
    <div class="container px-lg-5">
        <div class="row gy-5 gx-4 mt-3">
            <div class="col-md-2"></div>
            <div class="col-lg-7 col-md-7 wow fadeInUp " data-wow-delay="0.2s">
                <div class="position-relative shadow rounded border-top border-5 border-primary">
                    <div class="card-header">
                        <h3>Add Product</h3>
                    </div>
                    <div class=" border-bottom p-4 pt-5">

                        <form id="product_form" action="<?php echo URLROOT ?>/products/addproduct" method="POST">
                            <div class="form-group">
                                <label for="name">SKU </label>
                                <input type="text" name="sku" id="sku" required class="form-control">
                                <div class="text-danger skuErr"></div>
                            </div>

                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" name="name" id="name" required class="form-control">
                                <div class="text-danger nameErr"></div>
                            </div>

                            <div class="form-group mt-2">
                                <label for="price">Price</label>
                                <input type="number" name="price" id="price" required class="form-control">
                                <div class="text-danger priceErr"></div>

                            </div>

                            <div class="form-group mt-2">
                                <label for="price">Product Type</label>

                                <select name="productType" id="productType" class="form-control">
                                    <option value="Forniture">FORNITURE</option>
                                    <option value="Dvd">DVD</option>
                                    <option value="Book">BOOK</option>
                                </select>
                            </div>
                            <div class="row">
                                <div class="text-center mt-2">
                                    <h5 class="product-attr-title">Please, provide dimensions</h5>
                                </div>
                                <div class="col-md-2"></div>

                                <!-- Forniture attributes -->
                                <div class="col-md-8" id="forniture-attr">
                                    <div class="form-group mt-2 border p-2">
                                        Width (CM): <input type="number" name="width" id="width" class="form-control">
                                        <div class="text-danger widthErr"></div>

                                        Height (CM): <input type="number" name="height" id="height" class="form-control">
                                        <div class="text-danger heightErr"></div>

                                        Length (CM): <input type="number" name="length" id="length" class="form-control">
                                        <div class="text-danger lengthErr"></div>

                                    </div>
                                </div>

                                <!-- DVD attributes -->
                                <div class="col-md-8 hide" id="dvd-attr">
                                    <div class="form-group mt-2 border p-2">
                                        Size (MB): <input type="number" name="size" id="size" class="form-control">
                                        <div class="text-danger sizeErr"></div>

                                    </div>
                                </div>

                                <!-- Book attributes -->
                                <div class="col-md-8 hide" id="book-attr">
                                    <div class="form-group mt-2 border p-2">
                                        Weight (KG): <input type="number" name="weight" id="weight" class="form-control">
                                        <div class="text-danger weightErr"></div>

                                    </div>
                                </div>

                            </div>

                            <div class="row mt-3">

                                <div class="form-group col-md-6">
                                    <button type="submit" id="save" name="save" class="btn btn-primary">Save</button>
                                </div>
                                <div class="form-group col-md-6">
                                    <a href="<?php echo URLROOT ?>" class="btn btn-warning ">Cancel</a>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>

        </div>

    </div>

</div>
<!-- Product End -->
<?php
require APPROOT . '/views/includes/footer.php';
?>
<script>
    $(document).ready(function() {

        $('#width').attr('required', 'required');
        $('#height').attr('required', 'required');
        $('#length').attr('required', 'required');
        // Dynamically Show and Hide the attributes Box when Switch
        $('#productType').on('change', function() {
            if ($(this).val() == 'Dvd') {

                $('.product-attr-title').text('Please, provide size');
                $('#dvd-attr').removeClass('hide');
                $('#forniture-attr').addClass('hide');
                $('#book-attr').addClass('hide');
                $('#size').attr('required', 'required');

                $('#weight').removeAttr('required', 'required');
                $('#width').removeAttr('required', 'required');
                $('#height').removeAttr('required', 'required');
                $('#length').removeAttr('required', 'required');
                return false;
            } else if ($(this).val() == 'Book') {

                $('.product-attr-title').text('Please, provide weight');
                $('#book-attr').removeClass('hide');
                $('#forniture-attr').addClass('hide');
                $('#dvd-attr').addClass('hide');
                $('#weight').attr('required', 'required');

                $('#size').removeAttr('required', 'required');
                $('#width').removeAttr('required', 'required');
                $('#height').removeAttr('required', 'required');
                $('#length').removeAttr('required', 'required');

                return false;
            } else if ($(this).val() == 'Forniture') {

                $('.product-attr-title').text('Please, provide dimensions');
                $('#forniture-attr').removeClass('hide');
                $('#book-attr').addClass('hide');
                $('#dvd-attr').addClass('hide');

                $('#weight').removeAttr('required', 'required');
                $('#size').removeAttr('required', 'required');
                $('#width').attr('required', 'required');
                $('#height').attr('required', 'required');
                $('#length').attr('required', 'required');
                return false;

            }
        });
    });
</script>