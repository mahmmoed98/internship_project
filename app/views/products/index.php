 <?php
    // Include header of the page
    require APPROOT . '/views/includes/header.php';
    ?>

 <!-- Product Start -->
 <div class="container-xxl py-5">
     <div class="container px-lg-5">
         <div class="section-title position-relative text-center mx-auto mb-5 pb-4 wow fadeInUp" data-wow-delay="0.1s" style="max-width: 600px;">
             <h1 class="mb-3">Product List</h1>
         </div>

         <div class="row gy-5 gx-4" id="dynamic-products">
             <?php if ($data['products']) : ?>
                 <?php foreach ($data['products'] as $product) : ?>
                     <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.2s">
                         <div class="position-relative shadow rounded border-top border-5 border-primary">
                             <div class="text-center border-bottom p-4 pt-5">
                                 <div class="text-left" style="text-align: left;">
                                     <input type="checkbox" class="delete-checkbox" value="<?php echo $product->id; ?>">

                                 </div>
                                 <h4 class="fw-bold"><?php echo $product->sku; ?></h4>

                                 <h4 class="fw-bold"><?php echo $product->name; ?></h4>
                             </div>

                             <div class="p-4">
                                 <p class="border-bottom pb-3 text-center"><?php echo $product->price; ?>$</p>
                                 <p class="border-bottom pb-3 text-center"><i class="fa fa-check text-primary me-3"></i><?php echo $product->attribut_name; ?></p>
                             </div>
                         </div>
                     </div>
                 <?php endforeach ?>
             <?php
                else :
                    echo '<div class="col-md-12 text-center">No Products Found.</div>';
                endif; ?>
         </div>

     </div>

 </div>
 </div>
 <!-- Product End -->

 <!-- Footer Start -->
 <div class="container-fluid bg-primary text-white footer mt-5 pt-5 wow fadeIn" data-wow-delay="0.1s">

     <div class="container px-lg-5">
         <div class="copyright">
             <div class="row">
                 <div class="col-md-6 text-center text-md-start mb-3 mb-md-0">
                     &copy; <a class="border-bottom" href="#">MVC Project</a>, All Right Reserved.
                 </div>

             </div>
         </div>
     </div>
 </div>
 <!-- Footer End -->

 <?php
    // Include footer of the page
    require APPROOT . '/views/includes/footer.php';
    ?>

 <script>
     $(document).ready(function() {

         $('.delete-checkbox').change(function() {
             if ($(this).is(":checked")) {
                 $('#delete-checkbox').removeAttr('disabled');
             } else {
                 var deleteID = [];
                 $('.delete-checkbox:checked').each(function(i) {
                     deleteID[i] = $(this).val();
                 });
                 if(deleteID.length === 0){

                 $('#delete-checkbox').attr('disabled', 'disabled');
                 }

             }
         });

         // Mass Delete Products delete-checkbox
         $('#delete-checkbox').on('click', function() {

                
             var deleteID = [];
             $('.delete-checkbox:checked').each(function(i) {
                 deleteID[i] = $(this).val();
             });

             $.ajax({
                 url: "https://juniortest-mahmmoed-alkotob.000webhostapp.com/products/delete",
                 data: {
                     deleteID: deleteID
                 },
                 type: 'POST',
                 success: function(res) {
                     window.location = 'https://juniortest-mahmmoed-alkotob.000webhostapp.com';
                 }
             });
         });
     });
 </script>