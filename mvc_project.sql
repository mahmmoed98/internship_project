-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 25, 2022 at 05:59 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mvc_project`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `sku` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` varchar(20) NOT NULL,
  `product_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `sku`, `name`, `price`, `product_type`) VALUES
(3, '0', 'Numbers Don\'t Lie', '40', 'BOOK'),
(5, '0', 'Dr Web Disk', '90', 'DVD'),
(41, '0', 'The Book of Ampires', '300', 'BOOK'),
(42, '0', 'Dr Web Series', '500', 'DVD'),
(43, '0', 'Programing Chair', '500', 'FORNITURE'),
(46, '0', 'Man of Steel', '900', 'BOOK'),
(47, '0', 'One Man\'s army', '90', 'DVD'),
(48, '0', 'protest', '909', 'FORNITURE'),
(49, '0', 'slkdfskd', '8888', 'DVD'),
(50, '0', 'uiuu', '9999', 'FORNITURE'),
(51, '0', 'update_profile_action.php', '909', 'FORNITURE'),
(54, '234', 'ksfjksdj', '9', 'DVD'),
(55, '398', '', '', 'FORNITURE'),
(56, '387', 'jdhfjh', '909', 'FORNITURE'),
(57, '345', 'sdjksjd', '900990', 'FORNITURE'),
(58, '3453', 'Fahad Ullah', '900', 'FORNITURE'),
(59, '453', 'John Doe', '90', 'FORNITURE'),
(60, '1234', 'Fahad Ullah', '90', 'FORNITURE'),
(124, '123', 'Fahad Ullahkkk', '898', 'DVD'),
(125, '12322', 'test', '90', 'FORNITURE'),
(126, '787878778', '787', '7877', 'FORNITURE'),
(131, '9899', 'Product e', '90', 'FORNITURE');

-- --------------------------------------------------------

--
-- Table structure for table `product_attributes`
--

CREATE TABLE `product_attributes` (
  `id` int(11) NOT NULL,
  `attribut_name` varchar(255) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_attributes`
--

INSERT INTO `product_attributes` (`id`, `attribut_name`, `product_id`) VALUES
(3, 'Weight: 1Kg', 3),
(5, 'Size: 2GB', 5),
(17, 'Weight: 7KG', 41),
(18, 'Size : 900MB', 42),
(19, 'Dimensions: 900*200*300', 43),
(22, 'Weight: 5KG', 46),
(23, 'Size : 900MB', 47),
(24, 'Dimensions: 90*09*90', 48),
(25, 'Size: 800 MB', 49),
(26, 'Dimensions: 65*67*87', 50),
(27, 'Dimensions: 7*6*4', 51),
(30, 'Size: 6666 MB', 54),
(31, 'Dimensions: **', 55),
(32, 'Dimensions: **', 56),
(33, 'Dimensions: **', 57),
(34, 'Dimensions: **', 58),
(35, 'Dimensions: **', 59),
(36, 'Dimensions: **', 60),
(40, 'Size: 777 MB', 124),
(41, 'Dimensions: **', 125),
(42, 'Dimensions: 878*78*78', 126),
(47, 'Dimensions: 90*90*90', 131);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_attributes`
--
ALTER TABLE `product_attributes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=132;

--
-- AUTO_INCREMENT for table `product_attributes`
--
ALTER TABLE `product_attributes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `product_attributes`
--
ALTER TABLE `product_attributes`
  ADD CONSTRAINT `product_attributes_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
